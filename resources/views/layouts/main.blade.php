<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ngundangtamu</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="icon" href="https://partner.inv.co.id/storage/img/rekanan/i2EuvbkEzHHcCyLZi5Ss8tsxxJs1t6riZZYUJRLy.png" type="image/gif" sizes="16x16">
    </head>
    
    <body>
    @yield('container')
    </body>
    
</html>