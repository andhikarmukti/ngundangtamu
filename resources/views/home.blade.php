@extends('layouts.main')

@section('container')
<div class="embed-responsive embed-responsive-16x9" style="height:100vh">
    @if ($premium)
    <iframe class="embed-responsive-item" src="https://inv.co.id/demo-premium-wedding-{{ $premium }}"></iframe>
    @elseif($demo == null)
    <iframe class="embed-responsive-item" src="https://inv.co.id/ngundangtamu"></iframe>
    @else
    <iframe class="embed-responsive-item" src="https://inv.co.id/demo-{{ $demo }}"></iframe>
    @endif
</div>
@endsection