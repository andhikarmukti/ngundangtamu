<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home(Request $request){
        
        $demo = $request->demo;
        $premium = $request->premium;

        return view('home', [
            'demo' => $demo,
            'premium' => $premium
        ]);
    }

    public function basicDesign()
    {
        return view('basic-design',[
            'title' => 'Basic Design'
        ]);
    }

    public function premiumDesign()
    {
        return view('premium-design',[
            'title' => 'Premium Design'
        ]);
    }
}
